-- =============================================================================
-- -- Date :Juillet 2018
-- -- Objet : Module d'envoi de data de Centreon vers serveur externe
-- -- Auteur : Ghiles KHEDDACHE
-- --
-- --- Objet List : liste chainée maison ------------------------------------------
--
List = {}
function List.new()
    return {first = 0, last = -1}
end

function getHostname()
    local f = io.popen ("/bin/hostname")
    local hostname = f:read("*a") or ""
    f:close()
    hostname =string.gsub(hostname, "\n$", "")
    return hostname
end

--inserer a gauche -------------------------------------------------------------
function List.pushleft(list, value)
    local first = list.first - 1
    list.first = first
    list[first] = value
end

--inserer a droite -------------------------------------------------------------
function List.pushright(list, value)
    local last = list.last + 1
    list.last = last
    list[last] = value
    --print("element inserer")
end

--supprimer a gauche, le premier et renvoie le premier -------------------------
function List.popleft(list)
    local first = list.first
    if first > list.last then error("liste vide") end
    local value = list[first]
    list[first] = nil   -- a la corbeille
    list.first = first + 1
    return value
end

--supprimer a droite, le dernier -----------------------------------------------
function List.popright(list)
    local last = list.last
    if list.first > last then error("liste vide") end
    local value = list[last]
    list[last] = nil    -- a la corbeille
    list.last = last - 1
    return value
end

--affichage de la file ---------------------------------------------------------
function List.afficherList(list)
    local i=list.first
    while (list[i] ~= nil) do
      print(list[i])
      i=i+1
    end
end
-- =============================================================================

-- Initialisation du broker ---------------------------------------------------
function init(conf)
    broker_log:set_parameters(3, "/var/log/centreon-broker/stream_connector_KAFKA.log")
    limit = conf['limit']  --taille limite de la file configurée dans centreon
    host = conf['ip'] --ip de la destination (econtrol par ex)configurée dans centreon
    port = conf['port'] --port de destination configurée dans centreon
    polHostName = getHostname()
    -- FileOutput = "/tmp/FileOutput.stream" -- add file output for test
end
--------------------------------------------------------------------------------
---Fonction pour filtrer les données (services et host et ack)
function filter(category, element)
    if category == 1 and (element == 14 or element == 24 or element == 1 or element == 5) then
       return true
    end
    return false
end

--------------------------------------------------------------------------------
-------------Fonction pour envoyer les données
function senddata(data)
    -- local host  = "IP-PRODUCER KAFKA"
    -- local port = PORT PRDCER KAFKA
    local socket = broker_tcp_socket.new() -- création d'un socket
    socket:connect(host, port) -- établissement de la connexion avec le serveur
    if #data == 0 then return false end  --test si la chaine est vide ou pas
    -- Envoi des données
    if (socket:write(data)) == nil then --test de l'envoi et envoie en mm temps
        return false --erreur d'envoi, return false
    end
    --local content = socket:read() -- If, we want an answer, we also have a function to read
    socket:close() -- fermeture de la connexion à la fin de l'envoi
    return true
end
--------------------------------------------------------------------------------
function write(d)
    local maTable = {} ----------------tableau vide
    -- local maList = List.new()  ---creation list vide -------------
    local data  --variable pour stocker les données à envoyer
    -- local data2 = ""  --variable pour stocker des infos sur la file
    -- local limit = 3 --pour limiter la taille de la file
    local envoi = true

    if (maList == nil) then  --s'il n y a pas de file
        maList = List.new()  --création d'une file vide
        -- broker_log:info(3, "Creation d'une liste vide (globale)");
        -- data2 = "Creation d'une liste vide (globale)"
    end

    local host_name = broker_cache:get_hostname(d.host_id)  --récupère le nom de l'hôte

    if d.element == 24  or ( d.element == 1 and d.acknowledgement_type == 1 ) or ( d.element == 5 and d.type == 1 ) then  --teste si l'element est un service ou un ack de service ou un downtime de service
        local service_description = broker_cache:get_service_description(d.host_id, d.service_id) --récupère le nom du service
        -- local groupname = "" --variable pour stocker les noms des groupes de services
        -- local tabgroup = broker_cache:get_servicegroups(d.host_id, d.service_id) --récupère les groupes de services et stocke dans un tableau
        maTable["host_name"] = host_name
        maTable["service_description"] = service_description
    elseif  d.element == 14 or ( d.element == 1 and d.acknowledgement_type == 0 ) or ( d.element == 5 and d.type == 2 ) then
        maTable["host_name"] = host_name
    end

    maTable["pol_hostname"] = polHostName
    for i,v in pairs(d) do
        maTable[i] = v
    end

    data = broker.json_encode(maTable) .. "\n" --enodage des données en format json et stocke dans data

    --List.pushright(maList, data2) --on ajoute à droite
    List.pushright(maList, data) --on ajoute à droite dans la file
    -- broker_log:info(3, maList[maList.first])  --on ajoute la même dans le fichier de log
    -- ajout sortie vers un fichier
    -- local file = io.open(FileOutput, 'a')
    -- file:write(maList[maList.first])
    --  file:close()
    -------
    -- On essaye d'envoyer ce qu'il y a dans maList
    while ((envoi) and (maList[maList.first] ~= nil)) do
        if (pcall(senddata, maList[maList.first])) then --on essaye d'envoyer, on envoie l'élément à gauche
            envoi =  true  --on a reussi à envoyer
            List.popleft(maList)  --on dépile, on supprime à gauche
        elseif ((maList.last - maList.first +1) > limit) then --on teste si la file est trop grande
            List.popleft(maList)  --on dépile, on supprime à gauche (le plus ancien)
        else
            envoi = false --on ne dépile pas, on interrompt la boucle
        end
    end

    return true
end

